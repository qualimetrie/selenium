/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package mg.inclusiv.cdan007.selenuim;

import java.io.File;
import org.openqa.selenium.By;
import static org.openqa.selenium.By.id;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

/**
 *
 * @author djibo
 */
public class Selenium {
    public static void main(String[] args) {
       System.setProperty("webdriver.chrome.driver", "C:\\Users\\djibo\\Documents\\log\\chromedriver-win64\\chromedriver.exe");
        WebDriver d = new ChromeDriver();
        d.get("https://www.kibo.mg/");
        d.findElement(By.id("search_widget")).findElement(By.name("s")).sendKeys("cafe");
        d.findElement(By.id("search_widget")).findElement(By.name("s")).submit();
        d.findElement(By.cssSelector("#js-product-list > div.products.row > article:nth-child(1)")).click();
        d.findElement(By.cssSelector("#add-to-cart-or-refresh > div.product-add-to-cart > div > div.qty > div > span.input-group-btn-vertical > button.btn.btn-touchspin.js-touchspin.bootstrap-touchspin-up")).click();
        d.findElement(By.cssSelector("#add-to-cart-or-refresh > div.product-add-to-cart > div > div.add > button > span")).click(); 
        d.findElement(By.cssSelector("#blockcart-modal > div > div > div.modal-body > div > div.col-md-6.divide-left > div > div > a"));
        TakesScreenshot screenshot = (TakesScreenshot) d;
        File sourceFile = screenshot.getScreenshotAs(OutputType.FILE);
        File destinationFile = new File("C:\\Users\\djibo\\Documents\\screen1.png");
        
        // Copier le fichier source vers le fichier de destination
        // Cela peut nécessiter une gestion d'exception (IOException), assurez-vous d'ajouter cette gestion.
        sourceFile.renameTo(destinationFile);

        // Fermer le navigateur WebDriver
        d.quit();
    }

}